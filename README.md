# mongoose-array-validator

Add array validations for your mongoose schemas.

# Installation

```bash
npm install mongoose-array-validator
```

# Usage

```javascript
var mongoose = require('mongoose');
var arrayValidator = require('mongoose-array-validator');

const schema = new mongoose.Schema({
    myArray: {
        type: [String],
        minItems: 3,
        maxItems: 10, 
        uniqueItems: true
    }
});

schema.plugin(arrayValidator);

```

**minItems** is the minimum of the array length.

**maxItems** is the maximum of the array length.

**uniqueItems** defines if the values of the arrays must be unique (No duplicates allowed)

## With message callback

```javascript
const schema = new mongoose.Schema({
    myArray: {
       type: [String],
        minItems: {
            value: 2,
            message: props => `length of \`${props.path}\` (${props.value.length}) is less than allowed!`
        },
        maxItems: {
            value: 10,
            message: props => `length of \`${props.path}\` (${props.value.length}) is more than allowed!`
        },
        uniqueItems: {
            value: true,
            message: props => `No duplicates allowed!`
        }
    }
});
schema.plugin(arrayValidator);

```